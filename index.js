(function () {
    const shiftToLeft = (x, y) => {
        if (y < 0) return;
        let num = '';

        while (y--) {
            num += '0';
        }

        num += x.toString(2) + num;

        return parseInt(num, 2);
    };
})();
